<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Drone Monitoring</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Drone Monitoring</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div id="droneMonitoring">
                    <div class="row">
                        <div class="col">
                            <iframe width="100%" class="mainVideo" height="500" src="https://www.youtube.com/embed/ELLhVM8JGQ4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <ul class="listVideo">
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest1.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest2.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest3.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest1.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest2.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="">
                                        <img src="assets/img/forests/forest3.png" alt="">
                                        <h6>21 april 2020</h6>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->

<?php include_once "components/footer.php" ?>