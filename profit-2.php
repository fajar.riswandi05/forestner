<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Profit</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Profit</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div class="row">
                    <div class="col-md-6 col-sm-6 mb-3">
                        <div class="card chartSmall chartOrange">
                            <div class="content">
                                <h6>Total Pendapatan</h6>
                                <h2>Rp 38,6 M</h2>
                            </div>
                            <div id="totalPendapatan"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 mb-3">
                        <div class="card chartSmall chartRed">
                            <div class="content">
                                <h6>Total Pengeluaran</h6>
                                <h2>Rp 25,3 M</h2>
                            </div>
                            <div id="chartPengeluaran"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 mb-3">
                        <div class="card pt-4 pr-3 pb-3">
                            <div id="trend"></div>
                        </div>
                    </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->

<script>
    var optionTrend = {
        series: [{
            name: 'Pengeluaran',
            data: [31, 40, 28, 51, 42, 109, 100]
        }, {
            name: 'Pendapatan',
            data: [11, 32, 45, 32, 34, 52, 41]
        }],
        colors: ['#C24C4C', '#E99346'],
        chart: {
            height: 400,
            type: 'area',
            toolbar: {
                show: false
            },
            animations: {
                enabled: false
            },
            zoom: {
                enabled: false
            },
        },
        markers: {
            size: 5,
            hover: {
                size: 9
            }
        },
        legend: {
            show: true,
            showForSingleSeries: false,
            showForNullSeries: true,
            showForZeroSeries: true,
            position: 'top',
            horizontalAlign: 'left',
            floating: false,
            fontSize: '14px',
            fontFamily: 'Helvetica, Arial',
            fontWeight: 300,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            offsetX: 0,
            offsetY: 0,
            labels: {
                colors: '#777',
                useSeriesColors: false
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var trendChart = new ApexCharts(document.querySelector("#trend"), optionTrend);
    trendChart.render();

    var optionChartPendapatan = {
        series: [{
            name: 'series1',
            data: [31, 40, 28]
        }],
        colors: ['#546E7A', '#E91E63'],
        chart: {
            type: 'area',
            height: 160,
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: ['white', 'white'],
            width: 4,
            dashArray: 0,
        },
        markers: {
            size: 5,
            colors: ['#E19949'],
            strokeColors: 'white',
            hover: {
                size: 9
            }
        },
        fill: {
            opacity: 0.3,
            type: 'gradient',
            colors: ['#fff', '#fff'],
            gradient: {
                shade: 'light',
                type: "vertical",
                shadeIntensity: 0.9,
                gradientToColors: undefined, // optional, if not defined - uses the shades of same color in series
                inverseColors: true,
                opacityFrom: 0.6,
                opacityTo: 0.3,
                stops: [0, 50, 100],
                colorStops: []
            },
        },
        xaxis: {
            crosshairs: {
                width: 1
            },
        },
        yaxis: {
            min: 0
        },
    };

    var chartPendapatan = new ApexCharts(document.querySelector("#totalPendapatan"), optionChartPendapatan);
    chartPendapatan.render();


    var optionChartPengeluaran = {
        series: [{
            name: 'series1',
            data: [44, 88, 66]
        }],
        colors: ['#546E7A', '#E91E63'],
        chart: {
            type: 'area',
            height: 160,
            sparkline: {
                enabled: true
            },
        },
        markers: {
            size: 5,
            colors: ['#C24C4C'],
            strokeColors: 'white',
            hover: {
                size: 9
            }
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: ['white', 'white'],
            width: 4,
            dashArray: 0,
        },
        fill: {
            opacity: 0.3,
            type: 'gradient',
            colors: ['#fff', '#fff'],
            gradient: {
                shade: 'light',
                type: "vertical",
                shadeIntensity: 0.9,
                gradientToColors: undefined, // optional, if not defined - uses the shades of same color in series
                inverseColors: true,
                opacityFrom: 0.6,
                opacityTo: 0.3,
                stops: [0, 50, 100],
                colorStops: []
            },
        },
        xaxis: {
            crosshairs: {
                width: 1
            },
        },
        yaxis: {
            min: 0
        },
    };

    var chartPengeluaran = new ApexCharts(document.querySelector("#chartPengeluaran"), optionChartPengeluaran);
    chartPengeluaran.render();
</script>

<?php include_once "components/footer.php" ?>