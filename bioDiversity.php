<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Bio Diversity</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Bio Diversity</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div id="biodiversity" class="row mb-4">
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <a href="#">
                            <div class="card">
                                <div class="image">
                                    <img src="assets/img/macan.png" class="card-img-top" alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="mb-0">Macan Kumbang</h4>
                                    <ul>
                                        <li>Type : Animal</li>
                                        <li>Discovered : 50</li>
                                        <li>Rare : Yes</li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <a href="#">
                            <div class="card">
                                <div class="image">
                                    <img src="assets/img/raflesia.png" class="card-img-top" alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="mb-0">Raflesia Arnoldi</h4>
                                    <ul>
                                        <li>Type : Tumbuhan</li>
                                        <li>Discovered : 50</li>
                                        <li>Rare : Yes</li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <a href="#">
                            <div class="card">
                                <div class="image">
                                    <img src="assets/img/kijang.png" class="card-img-top" alt="...">
                                </div>
                                <div class="card-body">
                                    <h4 class="mb-0">Kijang</h4>
                                    <ul>
                                        <li>Type : Animal</li>
                                        <li>Discovered : 50</li>
                                        <li>Rare : Yes</li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <h4>Variation Of Trees</h4>
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Total Discovered</th>
                                <th>CO2 Reduce</th>
                                <th>02 Growth</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Teak Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                            <tr>
                                <td>Durian Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                            <tr>
                                <td>Mango Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                            <tr>
                                <td>Coconut Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                            <tr>
                                <td>Mangrove Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                            <tr>
                                <td>Bayan Trees</td>
                                <td>297</td>
                                <td>482LPM</td>
                                <td>3782LPM</td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->

<?php include_once "components/footer.php" ?>