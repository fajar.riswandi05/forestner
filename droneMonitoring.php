<?php include_once "components/head.php" ?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="assets/js/calendar.js"></script>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Drone Monitoring</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Drone Monitoring</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div id="droneMonitoring">
                    <div class="row">
                        <div class="col-md-5 col-sm-12 mb-3">
                            <div class="card p-3">
                                <div class="calendar-wrapper"></div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 mb-3">
                            <div class="card latesEventMonitoring">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Time</th>
                                            <th>Icon</th>
                                            <th>Attachment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Upaya Pemadaman</td>
                                            <td>21 January 2021, 08:45 WIB</td>
                                            <td class="d-flex">
                                                <span class="iconify" data-icon="tabler:drone" data-inline="false"></span>
                                                <span class="iconify" data-icon="medical-icon:i-fire-extinguisher" data-inline="false"></span>
                                            </td>
                                            <td class="text-center"><a href="#" data-toggle="modal" data-target="#modalVideo"><span class="iconify" data-icon="ant-design:play-circle-filled" data-inline="false"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Upaya Pemadaman</td>
                                            <td>21 January 2021, 08:45 WIB</td>
                                            <td class="d-flex">
                                                <span class="iconify" data-icon="tabler:drone" data-inline="false"></span>
                                                <span class="iconify" data-icon="medical-icon:i-fire-extinguisher" data-inline="false"></span>
                                            </td>
                                            <td class="text-center"><a href="#" data-toggle="modal" data-target="#modalVideo"><span class="iconify" data-icon="ant-design:play-circle-filled" data-inline="false"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Upaya Pemadaman</td>
                                            <td>21 January 2021, 08:45 WIB</td>
                                            <td class="d-flex">
                                                <span class="iconify" data-icon="tabler:drone" data-inline="false"></span>
                                                <span class="iconify" data-icon="medical-icon:i-fire-extinguisher" data-inline="false"></span>
                                            </td>
                                            <td class="text-center"><a href="#" data-toggle="modal" data-target="#modalVideo"><span class="iconify" data-icon="ant-design:play-circle-filled" data-inline="false"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Upaya Pemadaman</td>
                                            <td>21 January 2021, 08:45 WIB</td>
                                            <td class="d-flex">
                                                <span class="iconify" data-icon="tabler:drone" data-inline="false"></span>
                                                <span class="iconify" data-icon="medical-icon:i-fire-extinguisher" data-inline="false"></span>
                                            </td>
                                            <td class="text-center"><a href="#" data-toggle="modal" data-target="#modalVideo"><span class="iconify" data-icon="ant-design:play-circle-filled" data-inline="false"></span></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 mb-3">
                            <div class="card p-3">
                                <div class="mb-3">
                                    <h3>Drone Views</h3>
                                    <small>Last update on 21st April 2020</small>
                                </div>
                                <div class="row forestMonitoringImage">
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest1.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest2.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest3.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest1.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest2.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest1.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest3.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                    <div class="col-md-3 col-sm-12 imgConver">
                                        <img src="assets/img/forests/forest2.png" alt="" class="img-fluid img-radius">
                                        <h4>21 april <br>2021</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>

<!-- Modal Video -->
<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" aria-labelledby="modalVideoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalVideoLabel">Modal Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <iframe width="100%" class="mainVideo" height="500" src="https://www.youtube.com/embed/ELLhVM8JGQ4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!-- Modal Video -->

<!-- End Layout -->
<script>
    function selectDate(date) {
        $('.calendar-wrapper').updateCalendarOptions({
            date: date
        });
    }

    var defaultConfig = {
        weekDayLength: 1,
        date: new Date(),
        onClickDate: selectDate,
        showYearDropdown: true,
    };

    $('.calendar-wrapper').calendar(defaultConfig);
</script>
<?php include_once "components/footer.php" ?>