<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>My Forest</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">My Forest</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <!-- List Forests -->
                <ul class="listForest mb-5">
                    <li class="listItem">
                        <img src="assets/img/picture1.png" alt="Picture 1">
                        <div class="content">

                            <div class="title">West Borneo Rain Forest</div>

                            <div class="subtitle">
                                <span class="iconify" data-icon="feather:map-pin" data-inline="false"></span>
                                <span>Kapuas Hulu, Kec. Wakanda - 15 Hectare</span>
                            </div>

                            <div class="detail row p-0">
                                <div class="item col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item  col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item  col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                            </div>

                            <a href="" class="btn btn-primary mt-2 btn-block">Details</a>

                        </div>
                    </li>
                    <li class="listItem">
                        <img src="assets/img/picture2.png" alt="Picture 1">
                        <div class="content">

                            <div class="title">Kuala Satong Forest</div>

                            <div class="subtitle">
                                <span class="iconify" data-icon="feather:map-pin" data-inline="false"></span>
                                <span>Kuala Satong, Ketapang, West Borneo - 15 Hectare</span>
                            </div>

                            <div class="detail row p-0">
                                <div class="item col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item  col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item  col-4">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                            </div>

                            <a href="" class="btn btn-primary mt-2 btn-block">Details</a>

                        </div>
                    </li>
                </ul>
                <!-- List Forests -->

                <!-- Explore more forests -->
                <h4>Explore more forest</h4>
                <div class="row">

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest1.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest2.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest3.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest1.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest2.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest3.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest1.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 mb-3">
                        <div class="card">
                            <div class="image">
                                <img src="assets/img/forests/forest1.png" class="card-img-top" alt="...">
                                <span class="badge badge-primary-outline">800 Hectare</span>
                            </div>
                            <div class="card-body">
                                <h4 class="mb-0">Samboja Forest</h4>
                                <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                <div class="detail row p-0">
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                    <div class="item col-md-6">
                                        <div>
                                            <small>02</small>
                                            <span>50LPM</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Explore more forests -->

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>

<!-- Modal Forest Detail -->
<!-- Modal -->
<div class="modal fade modalForestDetail" id="modalForestDetail" tabindex="-1" aria-labelledby="modalForestDetailLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="row detailContainer">
                    <div class="col-lg-8 col-md-12 col-sm-12 p-0 carouselDetail">
                        <div id="carouselDetail" class="carousel carouselModalDetail slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselDetail" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselDetail" data-slide-to="1"></li>
                                <li data-target="#carouselDetail" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="assets/img/forestDetail1.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/img/forestDetail2.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/img/forestDetail3.png" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselDetail" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselDetail" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 sidebarDetail">
                        <div class="row title">
                            <div class="col-md-9 col-sm-12">
                                <h3>Forest Detail</h3>
                            </div>
                            <div class="col-md-3 col-sm-12"><img src="assets/img/avatar.png" class="img-fluid img-radius" alt=""></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 titleForestDetail">
                                <h5>Sebomban Bonti Rainforest (3502x Hectare)</h5>
                                <small class="d-flex">
                                    <span class="iconify mr-2 mt-1" data-icon="feather:map-pin" data-inline="false"></span>
                                    <span>Location : Sebomban bonti, Sanggau, West Borneo</span>
                                </small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 pl-4 pr-4 pt-0 pb-2">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7487497333727!2d110.57542651493465!3d0.33835656408378445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31fdbd9ba7dcf22b%3A0xcccc12956f1ddfba!2sBUKIT%20SEBOMBAN!5e0!3m2!1sen!2sid!4v1612803086979!5m2!1sen!2sid" width="100%" height="200" frameborder="0" style="border:0;border-radius:10px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                            <div class="col-md-12 detailAction pl-4 pr-4 pt-3 pb-2">
                                <h5 class="price">Rp 2.250.142.524</h5>
                                <a href="Checkout.php" class="btn btn-primary btn-block">Secure</a>
                                <small>By purchasing, you understand our management policy</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Forest Detail -->

<!-- End Layout -->

<?php include_once "components/footer.php" ?>