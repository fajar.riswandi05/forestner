<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Social Impact</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Social Impact</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div class="row">
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="card p-3">
                            <h3>Project air bersih</h3>
                            <div class="row">
                                <div class="col-md-4 col-sm-12"><img src="assets/img/airBersih.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                <div class="col-md-8 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    <div class="d-flex">
                                        <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                        <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="card p-3">
                            <h3>Orangutan COnservation</h3>
                            <div class="row">
                                <div class="col-md-4 col-sm-12"><img src="assets/img/orangUtan.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                <div class="col-md-8 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    <div class="d-flex">
                                        <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                        <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="card p-3">
                            <h3>Komodo Conservation</h3>
                            <div class="row">
                                <div class="col-md-4 col-sm-12"><img src="assets/img/komodo.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                <div class="col-md-8 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    <div class="d-flex">
                                        <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                        <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="card p-3">
                            <h3>Deer Conservation</h3>
                            <div class="row">
                                <div class="col-md-4 col-sm-12"><img src="assets/img/deer.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                <div class="col-md-8 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                    <div class="d-flex">
                                        <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                        <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ************* Main Content Here ***************** -->
        </div>
    </div>

</div>
</div>
<!-- End Layout -->

<?php include_once "components/footer.php" ?>