<?php include_once "components/head.php" ?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="assets/js/calendar.js"></script>


<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Overview</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Overview</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div class="row">
                    <div class="col-md-5 col-sm-12 mb-3">
                        <div class="card chartSmall chartRedGradient">
                            <div class="content">
                                <h6>Profit</h6>
                                <h2>Rp 38,6 M</h2>
                            </div>
                            <div id="chartProfit"></div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 mb-3">
                        <div class="card chartSmall pt-4 pr-4">
                            <div id="carbonChart"></div>
                        </div>
                    </div>

                    <div class="col-md-12 mt-3 mb-3">
                        <h3>Forest Monitoring</h3>
                    </div>

                    <div class="col-md-5 col-sm-12 mb-3">
                        <div class="card p-3">
                            <div class="calendar-wrapper"></div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 mb-3">
                        <div class="card forestMonitoringImage">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 imgConver">
                                    <img src="assets/img/forests/forest1.png" alt="" class="img-fluid img-radius">
                                    <h4>21 april <br>2021</h4>
                                </div>
                                <div class="col-md-6 col-sm-12 imgConver">
                                    <img src="assets/img/forests/forest2.png" alt="" class="img-fluid img-radius">
                                    <h4>21 april <br>2021</h4>
                                </div>
                                <div class="col-md-6 col-sm-12 imgConver">
                                    <img src="assets/img/forests/forest3.png" alt="" class="img-fluid img-radius">
                                    <h4>21 april <br>2021</h4>
                                </div>
                                <div class="col-md-6 col-sm-12 imgConver">
                                    <img src="assets/img/forests/forest1.png" alt="" class="img-fluid img-radius">
                                    <h4>21 april <br>2021</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-12 mb-3">
                        <h4>Latest Social Impact</h4>
                    </div>
                    
                    <div class="col-md-6 col-sm-12 mb-3">
                            <div class="card p-3">
                                <h3>Project air bersih</h3>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12"><img src="assets/img/airBersih.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                    <div class="col-md-8 col-sm-12">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <div class="d-flex">
                                            <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                            <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mb-3">
                            <div class="card p-3">
                                <h3>Project air bersih</h3>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12"><img src="assets/img/airBersih.png" class="img-fluid mr-3 img-radius img-md-size" alt=""></div>
                                    <div class="col-md-8 col-sm-12">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <div class="d-flex">
                                            <a href="" class="btn btn-primary mr-2">News (9+)</a>
                                            <a href="" class="btn btn-primary mr-2">Gallery (9+)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->

<script>
    var optionCarbon = {
        series: [{
            name: 'Co2 reduced',
            data: [31, 40, 28, 51, 42, 109, 100]
        }, {
            name: 'O2 growth',
            data: [11, 32, 45, 32, 34, 52, 41]
        }],
        colors: ['#F28B41', '#9F53CD'],
        chart: {
            height: 250,
            type: 'bar',
            toolbar: {
                show: false
            },
            animations: {
                enabled: false
            },
            zoom: {
                enabled: false
            },
        },
        markers: {
            size: 5,
            hover: {
                size: 9
            }
        },
        legend: {
            show: true,
            showForSingleSeries: false,
            showForNullSeries: true,
            showForZeroSeries: true,
            position: 'top',
            horizontalAlign: 'right',
            floating: false,
            fontSize: '14px',
            fontFamily: 'Helvetica, Arial',
            fontWeight: 300,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            offsetX: 0,
            offsetY: 0,
            labels: {
                colors: '#777',
                useSeriesColors: false
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var carbonChart = new ApexCharts(document.querySelector("#carbonChart"), optionCarbon);
    carbonChart.render();


    var optionProfit = {
        series: [{
            name: 'series1',
            data: [14, 20, 30]
        }],
        colors: ['#546E7A', '#E91E63'],
        chart: {
            type: 'area',
            height: 180,
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: ['white', 'white'],
            width: 4,
            dashArray: 0,
        },
        markers: {
            size: 5,
            colors: ['#E19949'],
            strokeColors: 'white',
            hover: {
                size: 9
            }
        },
        fill: {
            opacity: 0.3,
            type: 'gradient',
            colors: ['#fff', '#fff'],
            gradient: {
                shade: 'light',
                type: "vertical",
                shadeIntensity: 0.9,
                gradientToColors: undefined, // optional, if not defined - uses the shades of same color in series
                inverseColors: true,
                opacityFrom: 0.6,
                opacityTo: 0.3,
                stops: [0, 50, 100],
                colorStops: []
            },
        },
        xaxis: {
            crosshairs: {
                width: 1
            },
        },
        yaxis: {
            min: 0
        },
    };
    var chartProfit = new ApexCharts(document.querySelector("#chartProfit"), optionProfit);
    chartProfit.render();
</script>

<script>
    function selectDate(date) {
        $('.calendar-wrapper').updateCalendarOptions({
            date: date
        });
    }

    var defaultConfig = {
        weekDayLength: 1,
        date: new Date(),
        onClickDate: selectDate,
        showYearDropdown: true,
    };

    $('.calendar-wrapper').calendar(defaultConfig);
</script>

<?php include_once "components/footer.php" ?>