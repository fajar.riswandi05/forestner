<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="buyPage">
    <a href="#" class="logo"><img src="assets/img/logoWhite.svg" alt="Forestener"></a>
    <div class="container content">
        <h1>Grow your own <br> <span>forest</span></h1>

        <div class="action">
            <div><a href="myForest.php" class="btn btn-lg btn-rounded-primary-outline mr-3">Buy Now</a></div>
            <div>
                <small>Indonesia <span>from</span></small>
                <h4>Rp. 50.000/Ha</h4>
            </div>
        </div>
    </div>
</div>
<!-- End Layout -->

<?php include_once "components/footer.php" ?>