<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTopCheckOut.php" ?>

                <!-- ************* Main Content Here ***************** -->

                <div id="checkout" class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="itemForest">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <div class="card forestInfo">
                                        <div class="row align-items-center">
                                            <div class="col-md-3 col-sm-12 pl-4 pr-4"><img src="assets/img/forestDetail1.png" class="img-fluid" alt=""></div>
                                            <div class="auto p-4">
                                                <h3>Sebomban Bonti Rainforest <span>Hectare 3502x</span></h3>
                                                <small>
                                                    <span class="iconify" data-icon="feather:map-pin" data-inline="false"></span>
                                                    <span>Location : Sebomban bonti, Sanggau, West Borneo</span>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 mb-3">
                                    <div class="card forestSize p-3">
                                        <h5>Forest Size</h5>
                                        <p>
                                        <div class="input-group product_qty">
                                            <span class="input-group-btn">
                                                <button class="btn btn-white btn-minus" type="button">
                                                    <span class="iconify" data-icon="ant-design:minus-circle-filled" data-inline="false"></span>
                                                </button>
                                            </span>
                                            <input type="text" class="form-control no-padding add-color text-center height-25" maxlength="3" value="0">
                                            <span class="input-group-btn">
                                                <button class="btn btn-red btn-plus" type="button">
                                                    <span class="iconify" data-icon="ant-design:plus-circle-filled" data-inline="false"></span>
                                                </button>
                                            </span>
                                        </div>
                                        </p>
                                        <p><small>/ 3502 Hectares</small></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="card forestYears p-3">
                                        <h5>How Many Years ?</h5>
                                        <p>
                                        <div class="input-group product_qty">
                                            <span class="input-group-btn">
                                                <button class="btn btn-white btn-minus" type="button">
                                                    <span class="iconify" data-icon="ant-design:minus-circle-filled" data-inline="false"></span>
                                                </button>
                                            </span>
                                            <input type="text" class="form-control no-padding add-color text-center height-25" maxlength="3" value="0">
                                            <span class="input-group-btn">
                                                <button class="btn btn-red btn-plus" type="button">
                                                    <span class="iconify" data-icon="ant-design:plus-circle-filled" data-inline="false"></span>
                                                </button>
                                            </span>
                                        </div>
                                        </p>
                                        <p><small>/ max 50 Years</small></p>
                                    </div>
                                </div>

                                <div class="col-md-12 mt-3 mb-3">
                                    <div class="card detailCheckout">
                                        <div class="row align-items-center">
                                            <div class="col-md-4 col-sm-12 listPrice">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>Setup Fees</td>
                                                            <td>Rp 1.000.000</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Setup Fees</td>
                                                            <td>Rp 1.000.000</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Setup Fees</td>
                                                            <td>Rp 1.000.000</td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td><Strong>Total</Strong></td>
                                                            <td><Strong>Rp 23.000.000</Strong></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="col-md-8 col-sm-12 agreementBox">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">I agree checkbox. A user can click a box that's clearly marked as being part of forming an agreement.</label>
                                                </div>
                                                <a href="#" class="btn btn-primary btn-block mt-3">Secure Forest</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <h3 class="mb-3">Explore More</h3>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="card">
                                    <div class="image">
                                        <img src="assets/img/forests/forest1.png" class="card-img-top" alt="...">
                                        <span class="badge badge-primary-outline">800 Hectare</span>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="mb-0">Samboja Forest</h4>
                                        <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                        <div class="detail row p-0">
                                            <div class="item col-md-6">
                                                <div>
                                                    <small>02</small>
                                                    <span>50LPM</span>
                                                </div>
                                            </div>
                                            <div class="item col-md-6">
                                                <div>
                                                    <small>02</small>
                                                    <span>50LPM</span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <div class="card">
                                    <div class="image">
                                        <img src="assets/img/forests/forest2.png" class="card-img-top" alt="...">
                                        <span class="badge badge-primary-outline">800 Hectare</span>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="mb-0">Samboja Forest</h4>
                                        <small>Last Update on 21 Sept 2020, 08:00 PM</small>
                                        <div class="detail row p-0">
                                            <div class="item col-md-6">
                                                <div>
                                                    <small>02</small>
                                                    <span>50LPM</span>
                                                </div>
                                            </div>
                                            <div class="item col-md-6">
                                                <div>
                                                    <small>02</small>
                                                    <span>50LPM</span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-block btn-primary mt-3" data-toggle="modal" data-target="#modalForestDetail">Secure</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->



<?php include_once "components/footer.php" ?>
<script>
    $('.btn-minus').click(function() {
        $(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) - 1)
    })
    $('.btn-plus').click(function() {
        $(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) + 1)
    })
</script>