<?php include_once "components/head.php" ?>

<!-- Start Layout -->
<div class="container-fluid p-0">
    <div class="row">

        <!-- Sidebar Left -->
        <?php include_once "components/sidebarLeft.php" ?>

        <div class="col p-0">
            <div id="mainContent">

                <!-- Top navigation -->
                <?php include_once "components/NavigationTop.php" ?>

                <!-- Title pages -->
                <div class="titlePage">
                    <div>
                        <h1>Carbon Offset</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Carbon Offset</a></li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <span class="iconify mr-2 mt-1" data-icon="akar-icons:calendar" data-inline="false"></span>
                        <span>Last Update : 20 Aprl 2020</span>
                    </div>
                </div>
                <!-- End Title pages -->

                <!-- ************* Main Content Here ***************** -->

                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="card pt-4 pr-3 pb-3">
                            <div class="pl-3">
                                <h5>Track Co2 reduced and O2 Growth</h5>
                                <small>This Month, Jan 2020</small>
                            </div>
                            <div id="growth"></div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 mb-3">
                        <div class="card">
                            <div class="p-3">
                                <h5>1 Last Year</h5>
                                <small>Chart of carbon offset last year</small>
                                <div id="PieChart" class="mt-3"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="card p-3">
                            <div class="mb-3">
                                <h5>Carbon Offset Details</h5>
                                <small>Co2 reduced and O2 Growth details</small>
                            </div>
                            <div class="detailProfit">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex justify-content-between pt-2 pb-2 pl-3 pr-3 align-items-center">
                                        <div class="d-flex mr-3">
                                            <span class="mr-3"><strong>80LPM</strong></span>
                                            <span>O2 Growth</span>
                                        </div>
                                        <div>
                                            <div id="sparkline"></div>
                                        </div>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between pt-2 pb-2 pl-3 pr-3 align-items-center">
                                        <div class="d-flex mr-3">
                                            <span class="mr-3"><strong>80LPM</strong></span>
                                            <span>O2 Growth</span>
                                        </div>
                                        <div>
                                            <div id="sparkline2"></div>
                                        </div>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between pt-2 pb-2 pl-3 pr-3 align-items-center">
                                        <div class="d-flex mr-3">
                                            <span class="mr-3"><strong>80LPM</strong></span>
                                            <span>O2 Growth</span>
                                        </div>
                                        <div>
                                            <div id="sparkline3"></div>
                                        </div>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between pt-2 pb-2 pl-3 pr-3 align-items-center">
                                        <div class="d-flex mr-3">
                                            <span class="mr-3"><strong>80LPM</strong></span>
                                            <span>O2 Growth</span>
                                        </div>
                                        <div>
                                            <div id="sparkline4"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ************* Main Content Here ***************** -->
            </div>
        </div>

    </div>
</div>
<!-- End Layout -->

<script>
    var optionssparkline = {
        series: [{
            data: [33, 55, 14, 52, 72, 25, 27]
        }],

        chart: {
            type: 'area',
            height: 60,
            width: '100%',
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            curve: 'straight'
        },
        fill: {
            opacity: 1,
        },
        yaxis: {
            min: 0
        },
        colors: ['#9F53CD'],
    };
    var chartsparkline = new ApexCharts(document.querySelector("#sparkline"), optionssparkline);
    chartsparkline.render();

    var optionssparkline2 = {
        series: [{
            data: [33, 55, 14, 52, 72, 25, 27]
        }],

        chart: {
            type: 'area',
            height: 60,
            width: '100%',
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            curve: 'straight'
        },
        fill: {
            opacity: 1,
        },
        yaxis: {
            min: 0
        },
        colors: ['#9F53CD'],
    };
    var chartsparkline2 = new ApexCharts(document.querySelector("#sparkline2"), optionssparkline2);
    chartsparkline2.render();

    var optionssparkline3 = {
        series: [{
            data: [15, 61, 61, 73, 6, 29, 51]
        }],

        chart: {
            type: 'area',
            height: 60,
            width: '100%',
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            curve: 'straight'
        },
        fill: {
            opacity: 1,
        },
        yaxis: {
            min: 0
        },
        colors: ['#F28B41'],
    };
    var chartsparkline3 = new ApexCharts(document.querySelector("#sparkline3"), optionssparkline3);
    chartsparkline3.render();

    var optionssparkline4 = {
        series: [{
            data: [22, 71, 21, 35, 51, 61, 81]
        }],

        chart: {
            type: 'area',
            height: 60,
            width: '100%',
            sparkline: {
                enabled: true
            },
        },
        stroke: {
            curve: 'straight'
        },
        fill: {
            opacity: 1,
        },
        yaxis: {
            min: 0
        },
        colors: ['#9F53CD'],
    };
    var chartsparkline4 = new ApexCharts(document.querySelector("#sparkline4"), optionssparkline4);
    chartsparkline4.render();


    var Pieoptions = {
        series: [33, 55],
        colors: ['#9F53CD', '#F28B41'],
        chart: {
            width: 400,
            type: 'pie'
        },
        legend: {
            position: 'bottom'
        },
        labels: ['O2 growth', 'Co2 reduced'],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: '100%'
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };

    var Piechart = new ApexCharts(document.querySelector("#PieChart"), Pieoptions);
    Piechart.render();


    var optionGrowth = {
        series: [{
            name: 'Co2 reduced',
            data: [31, 40, 28, 51, 42, 109, 100]
        }, {
            name: 'O2 growth',
            data: [11, 32, 45, 32, 34, 52, 41]
        }],
        colors: ['#F28B41', '#9F53CD'],
        chart: {
            height: 400,
            type: 'bar',
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false
            },
        },
        markers: {
            size: 5,
            hover: {
                size: 9
            }
        },
        legend: {
            show: true,
            showForSingleSeries: false,
            showForNullSeries: true,
            showForZeroSeries: true,
            position: 'top',
            horizontalAlign: 'right',
            floating: false,
            fontSize: '14px',
            fontFamily: 'Helvetica, Arial',
            fontWeight: 300,
            formatter: undefined,
            inverseOrder: false,
            width: undefined,
            height: undefined,
            tooltipHoverFormatter: undefined,
            offsetX: 0,
            offsetY: 0,
            labels: {
                colors: '#777',
                useSeriesColors: false
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight'
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var growthChart = new ApexCharts(document.querySelector("#growth"), optionGrowth);
    growthChart.render();
</script>

<?php include_once "components/footer.php" ?>