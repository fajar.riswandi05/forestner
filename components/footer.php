<!-- Script -->

<script src="assets/js/jquery-3.5.1.slim.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

<!-- <script src="assets/js/bootstrap-datepicker.min.js"></script> -->
<!-- <link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css"/> -->
<!-- <script src="assets/js/sweetalert.min.js"></script> -->
<script>
    // Toggle button mobile trigger 
    $(document).ready(function() {
        $('.open-button').click(function() {
            $(this).toggleClass('open');
        });

        /* Menu fade/in out on mobile */
        $(".open-button").click(function(e) {
            e.preventDefault();
            $(".sidebarLeftContainer").toggleClass('open');
        });

    });

    /* Close menu using the escape key */
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            if ($(".sidebarLeftContainer").hasClass("open")) {
                $(".sidebarLeftContainer").removeClass("open")
            }

            if ($(".open-button").hasClass("open")) {
                $(".open-button").removeClass("open")
            }

            // if ($("body").hasClass("lock-scroll")) {
            //     $("body").removeClass("lock-scroll")
            // }
        }
    });
    $('.open-button').click(function() {

        if ($(this).hasClass('open')) {
            lockScroll(false);
        } else {
            lockScroll(true);
        }
    });

    function lockScroll(shouldLock) {

        if (shouldLock === undefined) {
            shouldLock = true;
        }

        if (shouldLock) {
            if (!$('body').hasClass('lock-scroll')) {
                $('body').addClass('lock-scroll');
            }
        } else {
            if ($('body').hasClass('lock-scroll')) {
                $('body').removeClass('lock-scroll');
            }
        }
    }
</script>

</body>

</html>