<div class="navigationTop">
    <div class="topLeft">
        <div class="dropdown dropdownTopNav">
            <a class="dropdown-toggle" href="#" role="button" id="idDropdownTopNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>West borneo rain forest</span>
            </a>

            <div class="dropdown-menu dropdownTopNavMenu" aria-labelledby="idDropdownTopNav">

                <ul class="listItem">
                    <li>
                        <img src="assets/img/picture1.png" alt="Picture 1">
                        <div class="content">

                            <div class="title">West Borneo Rain Forest</div>

                            <div class="subtitle">
                                <span class="iconify" data-icon="feather:map-pin" data-inline="false"></span>
                                <span>Kapuas Hulu, Kec. Wakanda - 15 Hectare</span>
                            </div>

                            <div class="detail row">
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                            </div>

                            <a href="" class="btn btn-primary mt-2 btn-block">Details</a>

                        </div>
                    </li>

                    <li>
                        <img src="assets/img/picture2.png" alt="Picture 1">
                        <div class="content">

                            <div class="title">Kuala Satong Forest</div>

                            <div class="subtitle">
                                <span class="iconify" data-icon="feather:map-pin" data-inline="false"></span>
                                <span>Kapuas Hulu, Kec. Wakanda - 15 Hectare</span>
                            </div>

                            <div class="detail row">
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                                <div class="item col">
                                    <div>
                                        <small>02</small>
                                        <span>50LPM</span>
                                    </div>
                                </div>
                            </div>

                            <a href="" class="btn btn-primary mt-2 btn-block">Details</a>

                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="topRight">
        <div>
            <a href="profile.php" class="profileSmall">
                <img src="assets/img/avatar.png" alt="Profile">
            </a>
        </div>
        <div class="toggleSidebarContainer">
            <button class="open-button toggleSidebar">
                <span class="iconify" data-icon="heroicons-outline:menu-alt-2" data-inline="false"></span>
            </button>
        </div>
    </div>
</div>