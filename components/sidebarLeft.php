<!-- Sidebar Left -->

<div class="col-md-2 p-0 sidebarLeftContainer">

    <div id="sidebarLeft">

        <a href="#" class="logo"><img src="assets/img/logoWhite.svg" alt="Forestener"></a>

        <ul class="navigationLeft">
            <li class="active">
                <a href="myForest.php">
                    <span class="iconify" data-icon="clarity:tree-line" data-inline="false"></span>
                    <span>Forests</span>
                </a>
            </li>
            <li>
                <a href="overview.php">
                    <span class="iconify" data-icon="grommet-icons:overview" data-inline="false"></span>
                    <span>Overview</span>
                </a>
            </li>
            <li>
                <a href="droneMonitoring.php">
                    <span class="iconify" data-icon="tabler:drone" data-inline="false"></span>
                    <span>Drone Monitoring</span>
                </a>
            </li>
            <li>
                <a href="bioDiversity.php">
                    <span class="iconify" data-icon="cil:dog" data-inline="false"></span>
                    <span>Bio Diversity</span>
                </a>
            </li>
            <li>
                <a href="socialImpact.php">
                    <span class="iconify" data-icon="ph:users" data-inline="false"></span>
                    <span>Social Impact</span>
                </a>
            </li>
            <li>
                <a href="profit.php">
                    <span class="iconify" data-icon="ant-design:line-chart-outlined" data-inline="false"></span>
                    <span>Profit</span>
                </a>
            </li>
            <li>
                <a href="carbonOffset.php">
                    <span class="iconify" data-icon="ant-design:dot-chart-outlined" data-inline="false"></span>
                    <span>Carbon Offset</span>
                </a>
            </li>
        </ul>

        <ul class="navigationBottom">
            <li>
                <a href="profile.php">
                    <span class="iconify" data-icon="bi:gear" data-inline="false"></span>
                    <span>Setting</span>
                </a>
            </li>
            <li>
                <a href="/">
                    <span class="iconify" data-icon="mdi:logout-variant" data-inline="false"></span>
                    <span>Logout</span>
                </a>
            </li>
        </ul>

    </div>
</div>

<!-- End Sidebar Left -->