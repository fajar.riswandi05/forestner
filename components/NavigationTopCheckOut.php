<div class="navigationTop">
    <div class="topLeft p-2 pl-4">
        <h3 class="mt-1">Checkout</h3>
    </div>
    <div class="topRight">
        <div>
            <a href="profile.php" class="profileSmall">
                <img src="assets/img/avatar.png" alt="Profile">
            </a>
        </div>
        <div class="toggleSidebarContainer">
            <button class="open-button toggleSidebar">
                <span class="iconify" data-icon="heroicons-outline:menu-alt-2" data-inline="false"></span>
            </button>
        </div>
    </div>
</div>